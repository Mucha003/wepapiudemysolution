﻿namespace WepApiUdemy.Api
{
    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;

    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();

            #region Creation migration DB
            //// Faire des MigrationsIIS
            //// En Azure pas besoin de ce code.
            //var _webHost = CreateWebHostBuilder(args).Build();

            //using (var scope = _webHost.Services.CreateScope())
            //{
            //    var services = scope.ServiceProvider;
            //    // On OBTIEN le service UdemyDBContext et on fait la migration.
            //    var _webApiUdemyContext = services.GetService<WebApiUdemyContexte>();
            //    _webApiUdemyContext.Database.Migrate();
            //}

            //_webHost.Run();
            #endregion
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((env, config) =>
            {
                //env => le nom d'environement ou en est(Ex: developpement, Production)
                //Config => Ou on realize la configuration des Providers.

                // Il prend Toujour le Dernier Ajouté ici dans l'ordre donc D'abord $"appsettings.{environement}.json puis l'autre
                // Du Bas en Haut
                var environement = env.HostingEnvironment.EnvironmentName;
                config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true); //reloadOnChange => kand il ya du changement dans le fichier recharge.
                config.AddJsonFile($"appsettings.{environement}.json", optional: true, reloadOnChange: true); //reloadOnChange => kand il ya du changement dans le fichier recharge.


                // key Vault (ici ils sont dans notre variable d'environnement)
                var curentConfig = config.Build();
                config.AddAzureKeyVault(curentConfig["Vault"], curentConfig["ClientId"], curentConfig["ClientSecret"]);

            }).UseStartup<Startup>();
    }
}
