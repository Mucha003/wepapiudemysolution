﻿namespace WepApiUdemy.Api.Services
{
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using WepApiUdemy.Api.Entities;

    // Pour consumer n'importe quelle service pour lequelle le cicle de vie sois Scoped a diff de transient.
    public class ConsumeScopedService : IHostedService, IDisposable
    {
        public IServiceProvider Services { get; }
        private Timer _timer;
        private readonly IHostingEnvironment environment;
        private readonly string fileName = "File 1.txt";

        public ConsumeScopedService(IServiceProvider services, IHostingEnvironment environment)
        {
            Services = services;
            this.environment = environment;
        }



        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(20));
            return Task.CompletedTask;
        }



        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }



        private void DoWork(object state)
        {
            using (var scope = Services.CreateScope())
            {
                // avec sa on obtien n'importe quelle service qui soit scoped
                var _db = scope.ServiceProvider.GetRequiredService<WebApiUdemyContexte>();

                var list = _db.Auteurs.ToList();

                foreach (var item in list)
                {
                    WriteToFile(item.Name+ "   " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
                }
            }
        }



        private void WriteToFile(string message)
        {
            var path = $@"{environment.ContentRootPath}\{fileName}";
            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine(message);
            }
        }



        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
