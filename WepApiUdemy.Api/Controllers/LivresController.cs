﻿namespace WepApiUdemy.Api.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using WepApiUdemy.Api.Entities;
    using Microsoft.EntityFrameworkCore;

    [Route("api/[controller]")]
    [ApiController]
    public class LivresController : ControllerBase
    {
        private readonly WebApiUdemyContexte _db;

        public LivresController(WebApiUdemyContexte db)
        {
            this._db = db;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Livre>>> Get()
        {
            var result = await this._db.Livres.Include(x => x.Auteur)
                                                .ToListAsync();

            return Ok(result); // 200
        }


        [HttpGet("{id}", Name = "GetLivre")]
        public async Task<ActionResult<Auteur>> Get(int id)
        {
            var result = await this._db.Livres.Include(x => x.Auteur)
                                                .FirstOrDefaultAsync(x => x.Id == id);

            if (result == null)
            {
                return NotFound(); // 404
            }

            return Ok(result);
        }


        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Livre model)
        {
            await this._db.Livres.AddAsync(model);
            await this._db.SaveChangesAsync();

            return new CreatedAtRouteResult("GetLivre", new { id = model.Id }, model);
        }


        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Livre model)
        {
            if (id != model.Id)
            {
                return BadRequest(); // 400
            }

            this._db.Entry(model).State = EntityState.Modified;
            await this._db.SaveChangesAsync();

            return Ok();
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var result = await this._db.Livres.FirstOrDefaultAsync(x => x.Id == id);

            if (result == null)
            {
                return NotFound(); // 404
            }

            this._db.Livres.Remove(result);
            await this._db.SaveChangesAsync();

            return Ok(result);
        }

    }
}
