﻿namespace WepApiUdemy.Api.Controllers
{
    using Models;
    using Entities;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Security.Claims;
    using System.IdentityModel.Tokens.Jwt;
    using Microsoft.IdentityModel.Tokens;
    using System.Text;

    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly UserManager<UserEntity> _userManager;
        private readonly SignInManager<UserEntity> _signManager;
        private readonly IConfiguration _configuration;


        public AccountsController(UserManager<UserEntity> UserManager, SignInManager<UserEntity> SignManager, IConfiguration Configuration)
        {
            this._userManager = UserManager;
            this._signManager = SignManager;
            this._configuration = Configuration;
        }


        [HttpPost("Register")]
        public async Task<ActionResult<UserToken>> Register([FromBody] UserInfo model)
        {
            var user = new UserEntity { UserName = model.Email, Email = model.Email };
            var result = await this._userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return BadRequest("Login/Password invalid");
            }

            return BuildToken(model);
        }


        [HttpPost("Login")]
        public async Task<ActionResult<UserToken>> Login([FromBody] UserInfo model)
        {
            var result = await this._signManager.PasswordSignInAsync(model.Email, model.Password, isPersistent: false, lockoutOnFailure: false);

            if (!result.Succeeded)
            {
                ModelState.AddModelError(string.Empty, "Invalid login Attemp.");
                return BadRequest(ModelState);
            }

            return BuildToken(model);
        }


        private UserToken BuildToken(UserInfo model)
        {
            var myClaim = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, model.Email),
                // jti: Identifier de manier Unique Un token
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                //new Claim("Key", "Value"),
            };

            // Cette Key Nous permet de Garantir l'authenticite de l'information qu'on recoit 
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Key"]));
            var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            // Temps D'expiration du Token, ici 1H
            var expiration = DateTime.UtcNow.AddHours(1);

            // Creation du Token
            JwtSecurityToken tokenCreated = new JwtSecurityToken(
                issuer: null,
                audience: null,
                claims: myClaim,
                expires: expiration,
                signingCredentials: cred);

            return new UserToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(tokenCreated),
                Expiration = expiration
            };
        }
    }
}