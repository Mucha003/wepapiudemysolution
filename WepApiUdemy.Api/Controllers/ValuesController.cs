﻿namespace WepApiUdemy.Api.Controllers
{
    // using Microsoft.AspNetCore.Authentication.JwtBearer;
    // using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.DataProtection;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Threading;

    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("PermettreSiteapirequest")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ValuesController : ControllerBase
    {
        private readonly IDataProtector _dataProtector;
        public ValuesController(IDataProtectionProvider protectionProvider)
        {
            this._dataProtector = protectionProvider.CreateProtector("Secret_Key");
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var protectorLimiteParLeTemp = this._dataProtector.ToTimeLimitedDataProtector();
            string text = "ici la valeur a crypter";
            string textCrypte = protectorLimiteParLeTemp.Protect(text, TimeSpan.FromSeconds(5));
            Thread.Sleep(6000);
            string textDecrypte = protectorLimiteParLeTemp.Unprotect(textCrypte); //erreur The payload expired at dateTime 

            //string text = "ici la valeur a crypter";
            //string textCrypte = this._dataProtector.Protect(text);
            //string textDecrypte = this._dataProtector.Unprotect(textCrypte);

            return Ok(new { text, textCrypte, textDecrypte });
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
