﻿namespace WepApiUdemy.Api.Controllers
{
    using Models;
    using Entities;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.AspNetCore.JsonPatch;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Linq;

    [Route("api/[controller]")]
    [ApiController]
    public class AuteursController : ControllerBase
    {
        private readonly WebApiUdemyContexte _db;
        private readonly IMapper _autoMapper;

        public AuteursController(WebApiUdemyContexte db, IMapper autoMapper)
        {
            this._db = db;
            this._autoMapper = autoMapper;
        }


        [HttpGet] // retourner un ActionResult(resp: code Status) ou une list D'AuteurDto
        public async Task<ActionResult<IEnumerable<AuteurDto>>> Get()
        {
            var result = await this._db.Auteurs.ToListAsync();
            var ListeDtoAuteurs = this._autoMapper.Map<List<AuteurDto>>(result);

            return Ok(ListeDtoAuteurs); // 200
        }


        [HttpGet("{id}", Name = "GetAuteur")]
        public async Task<ActionResult<AuteurDto>> Get(int id)
        {
            var auteur = await this._db.Auteurs.FirstOrDefaultAsync(x => x.Id == id);

            if (auteur == null)
            {
                return NotFound(); // 404
            }

            // il marche aussi avec les collection
            // ex: collection des Autors a AuteurDto;
            var auteurDto = this._autoMapper.Map<AuteurDto>(auteur);

            return Ok(auteurDto);
        }


        [HttpPost]
        public async Task<ActionResult> Post([FromBody] AuteurDto model)
        {
            // Si !modelValid BadRequest gere le [ApiController]

            var auteur = this._autoMapper.Map<Auteur>(model);

            await this._db.Auteurs.AddAsync(auteur);
            await this._db.SaveChangesAsync();

            var auteurDto = this._autoMapper.Map<AuteurDto>(model);

            //param 1: la route ou se trouve ge => Id 
            //param 2: Le param qui se trouvesur cette route(EndPoint)
            //param 3: La response(resultat)
            // Va ajouter et verifier s'il existe avec le Get(Id) endPoint et renvoi le model 
            //return new CreatedAtRouteResult("GetAuteur", new { id = model.Id }, model); // 201 
            return new CreatedAtRouteResult("GetAuteur", new { id = auteur.Id }, auteurDto); // car le get retur un AuteurDto
        }


        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] AuteurDto model)
        {
            // car le Dto na pas d'id il va le chercher dans entity quand il le map
            var auteur = this._autoMapper.Map<Auteur>(model);
            auteur.Id = id;

            this._db.Entry(auteur).State = EntityState.Modified;
            await this._db.SaveChangesAsync();
            
            return NoContent(); // renvoi rien 204
        }


        [HttpPatch("{id}")]
        public async Task<ActionResult> Patch(int id, [FromBody] JsonPatchDocument<AuteurDto> model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var auteurDB = await this._db.Auteurs.FirstOrDefaultAsync(x => x.Id == id);

            if (auteurDB == null)
            {
                return NotFound();
            }

            var auteurDto = this._autoMapper.Map<AuteurDto>(auteurDB);

            model.ApplyTo(auteurDto, ModelState);
            var isValid = TryValidateModel(auteurDB);

            if (!isValid)
            {
                return BadRequest(ModelState);
            }

            this._autoMapper.Map(auteurDto, auteurDB);
            await this._db.SaveChangesAsync();

            return NoContent();
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var auteurId = await this._db.Auteurs.Select(x => x.Id).FirstOrDefaultAsync(x => x == id);

            if (auteurId == default(int)) // default(int) car le select Id est un Int
            {
                return NotFound(); // 404
            }

            this._db.Auteurs.Remove(new Auteur { Id = auteurId });
            await this._db.SaveChangesAsync();

            return NoContent();
        }

    }
}
