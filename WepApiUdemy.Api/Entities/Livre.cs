﻿namespace WepApiUdemy.Api.Entities
{
    using System.ComponentModel.DataAnnotations;

    public class Livre
    {
        public int Id { get; set; }

        [Required]
        public string Titre { get; set; }

        [Required]
        public int AuteurId { get; set; }
        public Auteur Auteur { get; set; }
    }
}
