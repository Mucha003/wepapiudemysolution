﻿namespace WepApiUdemy.Api.Entities
{
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;


    public class WebApiUdemyContexte : IdentityDbContext<UserEntity>
    {
        public WebApiUdemyContexte(DbContextOptions<WebApiUdemyContexte> opt)
            : base(opt)
        {
        }

        public DbSet<Auteur> Auteurs { get; set; }
        public DbSet<Livre> Livres { get; set; }
    }
}
