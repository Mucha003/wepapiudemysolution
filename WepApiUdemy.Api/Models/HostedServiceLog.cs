﻿namespace WepApiUdemy.Api.Models
{
    public class HostedServiceLog
    {
        public int Id { get; set; }
        public string Message { get; set; }
    }
}
