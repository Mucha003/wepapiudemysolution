﻿namespace WepApiUdemy.Api.Models
{
    using System.ComponentModel.DataAnnotations;

    public class LivreDto
    {
        [Required]
        public string Titre { get; set; }

        [Required]
        public int AuteurId { get; set; }
    }
}