﻿namespace WepApiUdemy.Api.Models
{
    using System.ComponentModel.DataAnnotations;
    using WepApiUdemy.Api.Validators;

    public class AuteurUpdateDto
    {
        public int Id { get; set; }

        [Required]
        [FirstLetterUpper]
        [StringLength(10, ErrorMessage = "Ce champs doit avoir {1} caracteres")]
        public string Name { get; set; }

        [Range(18, 120)]
        public int Age { get; set; }

        [CreditCard]
        public string CreditCard { get; set; }

        [Url]
        public string Url { get; set; }
    }
}
