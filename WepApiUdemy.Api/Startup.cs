﻿namespace WepApiUdemy.Api
{
    using AutoMapper;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Tokens;
    using System;
    using System.Text;
    using WepApiUdemy.Api.Entities;
    using WepApiUdemy.Api.Models;
    using WepApiUdemy.Api.Services;


    public class Startup
    {
        public IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            // DataProtector
            services.AddDataProtection();


            // automapper
            //services.AddAutoMapper(typeof(Startup).Assembly);

            //// si conf personalise.
            services.AddAutoMapper(options =>
            {
                options.CreateMap<Auteur, AuteurDto>().ReverseMap();
            }, typeof(Startup).Assembly);


            // Identification Logins.(Default Conf)
            services.AddIdentity<UserEntity, IdentityRole>()
                .AddEntityFrameworkStores<WebApiUdemyContexte>()
                .AddDefaultTokenProviders();


            // Db config
            services.AddDbContext<WebApiUdemyContexte>(x =>
            {
                x.UseSqlServer(this._configuration.GetConnectionString("DefaultConnectionString"));
            });


            // Repositories
            services.AddTransient<Microsoft.Extensions.Hosting.IHostedService, ConsumeScopedService>();


            // Cors
            //services.AddCors();
            services.AddCors(opt =>
            {
                opt.AddPolicy("PermettreSiteapirequest", build =>
                build.WithOrigins("https://www.apirequest.io")
                     .WithMethods("GET", "POST")
                     .WithHeaders("*"));
            });


            // Token
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opt =>
                {
                    opt.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Key"])),
                        ClockSkew = TimeSpan.Zero
                    };
                });


            services.AddMvc()
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2) // Ignore les boucles des requettes
                    .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseCors();
            //app.UseCors(opt =>
            //    // Les AllowAny..... === *   => permet tout
            //    opt.WithOrigins("https://www.apirequest.io")
            //    .WithMethods("GET", "POST") // methodes des Actions que cette origin peut executer
            //    .WithHeaders("*") // * ==> tout (conten-type : Json, etc.....)
            //    );

            app.UseMvc();
        }

    }
}
