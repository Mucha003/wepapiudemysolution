﻿namespace WepApiUdemy.Api.Validators
{
    using System.ComponentModel.DataAnnotations;

    public class FirstLetterUpperAttribute : ValidationAttribute
    {
        /// <summary>
        ///    [nameAttr]
        ///    public string Name {get; set;}
        /// </summary>
        /// <param name="value">prend la value de la prop ou a ete place cette Attribut </param>
        /// <param name="validationContext">Info d'ou s'execute la validation </param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // ici la regle de validation
            if (value == null || string.IsNullOrEmpty(value.ToString()))
            {
                // faire sa car ici si la valuer est null il y a la prop [required]
                return ValidationResult.Success;
            }

            var firstLetter = value.ToString()[0].ToString();

            if (firstLetter != firstLetter.ToUpper())
            {
                return new ValidationResult("La premier Lettre doit etre en Majuscule");
            }

            return ValidationResult.Success;
        }
    }
}
