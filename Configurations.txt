						Configurations
					---------------------
Ex: AppSettings. (Json)
Ex: Variable d'environement. ==> var qui se trouve dans Systeme ou le soft s'execute.
								 In ne se trouve pas dans le code de notre Apps(MM pas dans Notre Git)
								 ControLPannel => System AND Security => System => Advance System Settings => Advance => environement Variable (Variable System)
								Open VS Admin
	Azure => Application Settings (environement Variable)
			 Azure Key Vault


Pour les Variables d'environnement.
	- Dans Program
	
	        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((env, config) =>
            {
                //env => le nom d'environement ou en est(Ex: developpement, Production)
                //Config => Ou on realize la configuration des Providers.

                var environement = env.HostingEnvironment.EnvironmentName;
                config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true); //reloadOnChange => kand il ya du changement dans le fichier recharge.
                config.AddJsonFile($"appsettings.{environement}.json", optional: true, reloadOnChange: true); //reloadOnChange => kand il ya du changement dans le fichier recharge.

            }).UseStartup<Startup>();


Pour lire ces Information on utilise IConfiguration.
ctor(IConfiguration configuration)
	- _confifuration["Key"]
	- _confifuration["ConnectionStrings:DefaultConnectionString"]