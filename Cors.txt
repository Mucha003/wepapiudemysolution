						Cors
					-----------
Policy => permet de centraliser nos congig cors en un seul endroid

					Configuration au Niveau MiddelWare
			------------------------------------------------
1. services.AddCors();

2. app.UseCors(opt =>
    // Les AllowAny..... === *   => permet tout
    opt.WithOrigins("https://www.apirequest.io")
    .WithMethods("GET", "POST") // methodes des Actions que cette origin peut executer
    .WithHeaders("*") // * ==> tout (conten-type : Json, etc.....)
    );

Nous pouvons aussi gerer cors par attribut [] 
dasn les controllers ou accions pour gerer nos policy.


					Policy : Configuration au Niveau Attributs
				--------------------------------------------------
1. services.AddCors(opt =>
            {
                opt.AddPolicy("PermettreSiteapirequest", build =>
                build.WithOrigins("https://www.apirequest.io")
                     .WithMethods("GET", "POST")
                     .WithHeaders("*"));
            });

2. app.UseCors();

3. Controller ou Actions (Marche comme AllowAnonymous)
	- [EnableCors("PermettreSiteapirequest")]
   
   Si on le met au niveau d'une Accion
   Le site aurra acce que sur cette Accion.

   ex : [EnableCors("PermettreSiteapirequest")]
		public ActionResult<string> Get(int id) { }